# Gitlab Registry Cleaner

This tool helps interaction with Gitlab Registry Delete API to keep a clean registry.

Is already made to be run inside a Gitlab CI job so it will try to take all base config from Gitlab CI ENV vars

### WARNING
Deleting tags using this tool or Gitlab UI is not enough to reclaim disk space, 
a cron should be configured to run `gitlab-ctl registry-garbage-collect -m` as `root` as documented [HERE](https://docs.gitlab.com/omnibus/maintenance/#removing-unused-layers-not-referenced-by-manifests)

## Setup
### Local
#### Requirements
  - poetry
  - pyenv
  - python >= 3.7
#### Install
  - `pyenv install 3.7.0`
  - `pyenv virtualenv 3.7.0 gitlab-registry-cleaner`
  - `pyenv local gitlab-registry-cleaner`
  - `poetry install`

### Docker
#### Requirements
 - docker

#### Build
`docker build -t ovalmoney/gitlab-registry-cleaner:latest .`

#### Run
`docker run -it --rm ovalmoney/gitlab-registry-cleaner:latest`

## Usage
### WARNING
If run without options by default it will delete ALL images tags of all repositories of the specified project except `latest`.

To see a complete list of options `gitlab-registry-cleaner --help`

Gitlab Registry Cleaner accepts command options or ENV vars

### Options

| option | env var | type | required | default | desc |
|--------|---------|------|----------|---------|------|
| --api-url | TEXT | GRC_API_V4_URL, CI_API_V4_URL | * |  | Gitlab API Url (e.g. https://example.com:3000/api/v4) |
| --token | TEXT | GRC_TOKEN | * | | Gitlab API Token |
| --project-id | INT | GRC_PROJECT_ID, CI_PROJECT_ID | * |  | Gitlab Project ID |
| --repository-name | REGEX | GRC_REPOSITORY_NAME |  | .* | Gitlab Registry Repository Name |
| --tag-name | REGEX | GRC_TAG_NAME |  | .* | Tag names to delete |
| --keep-n | INT | GRC_KEEP_N |  | 0 | Keep last N tags |
| --older-than | TEXT | GRC_OLDER_THAN |  |  | Delete tags older than (e.g. 1h) |
| --dry-run | BOOL |  |  | False | Log what would do without doing it |

## Gitlab CI Example

`gitlab-ci.yml`
```yaml
clean_registry:on-schedule:
  image: 
    name: ovalmoney/gitlab-registry-cleaner:latest
  only:
    - schedules
  script:
    - gitlab-registry-cleaner --keep-n 5 --older-than 1w
```
**N.B. ATM the CI_JOB_TOKEN is read-only, so you should create a personal token (with role `maintainer`) and add it to the CI ENV vars as `GRC_TOKEN`**
