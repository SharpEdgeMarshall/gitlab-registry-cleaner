import logging
import click

from .gitlab import GitlabRegistryCleaner

FORMAT = '%(message)s'
logging.basicConfig(format=FORMAT)
logger = logging.getLogger()
logger.setLevel(logging.INFO)

@click.command()
@click.option('--api-url', 'api_url', required=True, envvar=["GRC_API_V4_URL", "CI_API_V4_URL"], help='Gitlab API Url (e.g. https://example.com:3000/api/v4)')
@click.option('--token', 'token', required=True, envvar=["GRC_TOKEN"], help='Gitlab API Token')
@click.option('--project-id', 'project_id', required=True, type=int, envvar=["GRC_PROJECT_ID", "CI_PROJECT_ID"], help='Gitlab Project ID')
@click.option('--repository-name', 'repository_name', default=".*", show_default=True, envvar=["GRC_REPOSITORY_NAME"], help='Gitlab Registry Repository Name Type: Regex')
@click.option('--tag-name', 'tag_name', default=".*", show_default=True, envvar=["GRC_TAG_NAME"], help='Tag names to delete Type: Regex')
@click.option('--keep-n', 'keep_n', default=0, show_default=True, type=int, envvar=["GRC_KEEP_N"], help='Keep last N tags')
@click.option('--older-than', 'older_than', default="", show_default=True, envvar=["GRC_OLDER_THAN"], help='Delete tags older than (e.g. 1h)')
@click.option('--dry-run', 'dry_run', is_flag=True)
def main(
    api_url,
    token,
    project_id,
    repository_name,
    tag_name,
    keep_n,
    older_than,
    dry_run,
):

    _grc = GitlabRegistryCleaner(api_url, token, dry_run)
    
    _grc.clean_registry_repositories(
        project_id,
        repository_name,
        tag_name,
        keep_n,
        older_than
    )


if __name__ == '__main__':
    main()
